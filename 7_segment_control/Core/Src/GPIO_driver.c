/******************************************************************************
* Includes
******************************************************************************/
#include <stdbool.h>
#include "GPIO_driver.h"

/******************************************************************************
* Private definitions and macros
******************************************************************************/

/******************************************************************************
* Private types
******************************************************************************/
typedef void (*clk_enable_func_t) (uint32_t clk_port);
typedef uint32_t (*is_clk_enable_func_t) (uint32_t clk_port);

typedef struct sGpioPinConfig_t {
    GPIO_TypeDef         *port;
    uint32_t             pin;
    uint32_t             mode;
    uint32_t             speed;
    uint32_t             output_type;
    uint32_t             pull;
    is_clk_enable_func_t check_func_name;
    clk_enable_func_t    func_name;
    uint32_t             clk_port;
    bool                 is_default_state_high;
} sGpioPinConfig_t;
/******************************************************************************
* Private constants
******************************************************************************/

/******************************************************************************
* Private variables
******************************************************************************/
const static sGpioPinConfig_t gpio_init_table[eGpioPin_Last] = {
        [eGpioPin_A0] =           {.port = GPIOA, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A1] =           {.port = GPIOA, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A2] =           {.port = GPIOA, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A3] =           {.port = GPIOA, .pin = LL_GPIO_PIN_3, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A4] =           {.port = GPIOA, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A5] =           {.port = GPIOA, .pin = LL_GPIO_PIN_5, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A6] =           {.port = GPIOA, .pin = LL_GPIO_PIN_6, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
        [eGpioPin_A7] =           {.port = GPIOA, .pin = LL_GPIO_PIN_7, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output_type = LL_GPIO_OUTPUT_PUSHPULL, .pull = 0, .check_func_name = LL_APB2_GRP1_IsEnabledClock, .func_name = LL_APB2_GRP1_EnableClock, .clk_port = LL_APB2_GRP1_PERIPH_GPIOA, .is_default_state_high = false},
    };
/******************************************************************************
* Exported variables and references
******************************************************************************/

/******************************************************************************
* Prototypes of private functions
******************************************************************************/

/******************************************************************************
* Definitions of private functions
******************************************************************************/

/******************************************************************************
* Definitions of exported functions
******************************************************************************/
bool GPIO_DRIVER_Init (void) {
    LL_GPIO_InitTypeDef gpio_init_struct = {0};
    bool is_all_gpio_init = true;

    for (uint8_t i = 0; i < ENTRIES_IN_ARRAY(gpio_init_table); i++) {
        if (gpio_init_table[i].check_func_name(gpio_init_table[i].clk_port) == 0) {
            gpio_init_table[i].func_name(gpio_init_table[i].clk_port);
        }
        gpio_init_struct.Pin = gpio_init_table[i].pin;
        gpio_init_struct.Mode = gpio_init_table[i].mode;
        gpio_init_struct.Speed = gpio_init_table[i].speed;
        gpio_init_struct.OutputType = gpio_init_table[i].output_type;
        gpio_init_struct.Pull = gpio_init_table[i].pull;

        if (LL_GPIO_Init(gpio_init_table[i].port, &gpio_init_struct) != SUCCESS) {
       	    is_all_gpio_init = false;
        }

        GPIO_DRIVER_SetDefaultPinState(i);
    }
    return is_all_gpio_init;
}

bool GPIO_DRIVER_Set (eGpioPin_t pin) {
    bool is_gpio_set = false;
    if (pin < eGpioPin_Last) {
        LL_GPIO_SetOutputPin(gpio_init_table[pin].port, gpio_init_table[pin].pin);
        is_gpio_set = true;
    }
    return is_gpio_set;
}

bool GPIO_DRIVER_Reset (eGpioPin_t pin) {
    bool is_gpio_reset = false;
    if (pin < eGpioPin_Last) {
        LL_GPIO_ResetOutputPin(gpio_init_table[pin].port, gpio_init_table[pin].pin);
        is_gpio_reset = true;
    }
    return is_gpio_reset;
}

bool GPIO_DRIVER_Toggle (eGpioPin_t pin) {
    bool is_gpio_toggled = false;
    if (pin < eGpioPin_Last) {
        LL_GPIO_TogglePin(gpio_init_table[pin].port, gpio_init_table[pin].pin);
        is_gpio_toggled = true;
    }
    return is_gpio_toggled;
}

uint8_t GPIO_DRIVER_Read (eGpioPin_t pin) {
    return (uint8_t)LL_GPIO_IsInputPinSet(gpio_init_table[pin].port, gpio_init_table[pin].pin);;
}

bool GPIO_DRIVER_SetDefaultPinState (eGpioPin_t pin) {
    bool is_set_successful = false;
    if (pin < eGpioPin_Last) {
        if (gpio_init_table[pin].is_default_state_high) {
            GPIO_DRIVER_Set(pin);
        } else {
            GPIO_DRIVER_Reset(pin);
        }
        is_set_successful = true;
    }
    return is_set_successful;
}
