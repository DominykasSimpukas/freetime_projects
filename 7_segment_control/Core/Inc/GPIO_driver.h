#ifndef GPIO_driver_H
#define GPIO_driver_H

/******************************************************************************
* Includes
******************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "stm32f1xx_ll_gpio.h"
#include "stm32f1xx_ll_bus.h"
/******************************************************************************
* Exported definitions and macros
******************************************************************************/
#define ENTRIES_IN_ARRAY(array)	(sizeof(array)/sizeof(array[0]))
/******************************************************************************
* Exported types
******************************************************************************/
typedef enum {
    eGpioPin_A0 = 0,
    eGpioPin_A1,
    eGpioPin_A2,
    eGpioPin_A3,
    eGpioPin_A4,
    eGpioPin_A5,
    eGpioPin_A6,
    eGpioPin_A7,
    eGpioPin_Last
} eGpioPin_t;
/******************************************************************************
* Exported variables
******************************************************************************/

/******************************************************************************
* Prototypes of exported functions
******************************************************************************/
bool GPIO_DRIVER_Init                 (void);
bool GPIO_DRIVER_Set                  (eGpioPin_t pin);
bool GPIO_DRIVER_Reset                (eGpioPin_t pin);
bool GPIO_DRIVER_Toggle               (eGpioPin_t pin);
uint8_t GPIO_DRIVER_Read              (eGpioPin_t pin) ;
bool GPIO_DRIVER_SetDefaultPinState   (eGpioPin_t pin);
#endif /*GPIO_driver_H*/
